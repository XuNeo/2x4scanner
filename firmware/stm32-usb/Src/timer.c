#include "stm32f0xx.h"
#include "timer.h"
#include "stdbool.h"

#define TIMER_LIST_MAX  10
struct _timer_list{
  bool enable;
  void (*callback)(void);
  uint32_t tick;
  uint32_t next_alarm_tick; //the next alarm time
}timer_list[TIMER_LIST_MAX];

static uint32_t list_len = 0;
static uint32_t curr_tick = 0;
static uint32_t time_per_tick = 1;  //time in ms per tick.

void timer_init(uint32_t period_ms){
  time_per_tick = 1;
}

void timer_register(void (*call_back)(void), uint32_t period_ms){
  if(call_back == 0) return;
  uint32_t tick = period_ms/time_per_tick;
  for(uint32_t i=0; i< list_len; i++){
    if(timer_list[i].enable == false ||
        timer_list[i].callback == call_back){
      //insert this callback here and return
      timer_list[i].callback = call_back;
      timer_list[i].tick = tick;
      timer_list[i].next_alarm_tick = curr_tick + tick;
      timer_list[i].enable = true;
      return;
    }
  }
  if(list_len == TIMER_LIST_MAX)
    return;
  //add it to end of list
  timer_list[list_len].callback = call_back;
  timer_list[list_len].tick = tick;
  timer_list[list_len].next_alarm_tick = curr_tick + tick;
  timer_list[list_len].enable = true;
  list_len ++;
}

void timer_unlink(void (*call_back)){
  for(uint32_t i=0; i< list_len; i++){
    if(timer_list[i].callback == call_back){
      //replace it with latest tick value and return.
      timer_list[i].enable = false;
      timer_list[i].next_alarm_tick = 0;
      return;
    }
  }
}

void HAL_SYSTICK_Callback(void)
{
  curr_tick++;  //it need ~50days until it overflow if period is 1ms.
  for(uint32_t i=0; i<list_len; i++){
    if(timer_list[i].enable == true)
    if(curr_tick > timer_list[i].next_alarm_tick){
      timer_list[i].next_alarm_tick += timer_list[i].tick;
      timer_list[i].callback(); //call function
    }
  }
}
