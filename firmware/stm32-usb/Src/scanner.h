#ifndef _SCANNER_H_
#define _SCANNER_H_
#include "stm32f0xx.h"

enum{
  SCANNER_OFF = 0,    //no channel is selected.
  SCANNER_CHAN0,
  SCANNER_CHAN1,
  SCANNER_CHAN2,
  SCANNER_CHAN3,
};

void scanner_init(void);
void scanner_set_chan(uint32_t chan);

#endif
