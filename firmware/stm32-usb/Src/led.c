#include "stm32f0xx.h"
#include "timer.h"

#define led_on()  GPIOA->BRR = GPIO_PIN_4
#define led_off() GPIOA->BSRR = GPIO_PIN_4
#define led_tgl() GPIOA->ODR ^= GPIO_PIN_4

static uint32_t led_disp_num = 0;
static uint32_t led_blink_speed_mask = 0x1f;
static uint32_t led_blink_period = 200;
void led_timer(void){
  if(led_disp_num == 0)
    return;
  static uint32_t timer_count, blink_count;
  timer_count++;
  if((timer_count&led_blink_speed_mask) == 0){
    if(blink_count < led_disp_num)
      led_on();
    blink_count++;
  }
  else{
    led_off();
  }
  if(timer_count == led_blink_period){
    blink_count = 0;
    timer_count = 0;
  }
}

void led_hw_init(void){
  GPIO_InitTypeDef  GPIO_InitStruct;
  __HAL_RCC_GPIOA_CLK_ENABLE();
  GPIO_InitStruct.Pin       = GPIO_PIN_4;
  GPIO_InitStruct.Mode      = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull      = GPIO_NOPULL;
  GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_HIGH;
  GPIO_InitStruct.Alternate = 0;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
  led_on();
  timer_register(led_timer, 10);
}

void led_disp(uint32_t num){
  if(num == 0){
    led_off();
    led_disp_num = num;
  }
  else if (num < 5)
  {
    led_disp_num = num;
  }
  else{
    led_disp_num = 0;
    led_on();
  }
}

#include "ush.h"
#include "string.h"

int32_t led(uint32_t argc, uint8_t *argv[]){
  ush_num_def num_type;
  ush_error_def error;
  uint32_t value;
  if(argc != 2)
    return -1;
  error = ush_str2num(argv[1], strlen((const char*)argv[1]), &num_type, &value);
  if(error != ush_error_ok)
    return -2;
  if(num_type != ush_num_uint32)
    return -3;
  led_disp(value);
  printf("\ndisplaying %d\n", value);
  return 0;
}
USH_REGISTER(led  , led, display number using led);
