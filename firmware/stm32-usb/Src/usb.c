#include "usb.h"
#include "usb_device.h"
#include "usbd_cdc_if.h"
#include "fifo.h"
#include "ush.h"

fifo_def fifo_rx, fifo_tx;
ush_def ush;
uint8_t temp[64];

uint8_t rxbuff[128], txbuff[384], ushbuff[64];
void usb2stream_init(void){
	fifo_init(&fifo_rx, rxbuff, 128);
	fifo_init(&fifo_tx, txbuff, 384);
	//parser init
  ush_init(&ush, ushbuff, 64);
}
/**
 * Init usb related resource.
*/
void usb_init(void){
  MX_USB_DEVICE_Init();
	usb2stream_init();
}

/**
 * Try to transmit data if USB if free and we have some data.
*/
static void usb_tx_try(void){
  uint32_t data_count;
  if(CDC_is_busy())
    return;
  data_count = fifo_status(&fifo_tx);
  if(data_count > 16){  //we have some data in fifo, transmit them now.
    data_count = data_count>63?63:data_count;
    fifo_read(&fifo_tx, temp, &data_count);
    CDC_Transmit_FS(temp, data_count);
  }
}

/**
 * Deal with usb tasks.
*/
char flag_go = 0;
void usb_poll(void){
  uint32_t data_count;
  //try to transmit data
  if(CDC_is_busy() == 0)
	{
    data_count = fifo_status(&fifo_tx);
		if(data_count){
      data_count = data_count>63?63:data_count;
			fifo_read(&fifo_tx, temp, &data_count);
			while(CDC_Transmit_FS(temp, data_count) == USBD_BUSY);
		}
  }

  //process received data
  data_count = fifo_status(&fifo_rx);
  if(data_count){
    data_count = data_count>64?64:data_count;
    fifo_read(&fifo_rx, temp, &data_count);
    ush_process_input(&ush, temp, data_count);
  }
}

void _putchar(char character)
{
  fifo_write1B(&fifo_tx, character);
  usb_tx_try();
}
