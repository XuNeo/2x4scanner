#include "relays.h"
#include "stm32f0xx.h"

#define RELAY_CS_L()  GPIOB->BRR = GPIO_PIN_1
#define RELAY_CS_H()  GPIOB->BSRR = GPIO_PIN_1

static uint32_t relay_curr_sta; /**< Current relay settings status. */
SPI_HandleTypeDef  SpiHandle;

/**
 * @brief init relay related hardware including GPIO and SPI.
 * @return none.
*/
void relay_hardware_init(void)
{
  GPIO_InitTypeDef  GPIO_InitStruct;

  //PB1-->CS PA5-->SCLK, PA7-->MOSI
  /* Enable the SPI periph */
  __HAL_RCC_SPI1_CLK_ENABLE();

  /* Enable SCK, MOSI, MISO and NSS GPIO clocks */
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  GPIO_InitStruct.Pin       = GPIO_PIN_5;
  GPIO_InitStruct.Mode      = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull      = GPIO_PULLDOWN;
  GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_HIGH;
  GPIO_InitStruct.Alternate = GPIO_AF0_SPI1;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
  
  /* SPI MOSI GPIO pin configuration  */
  GPIO_InitStruct.Pin = GPIO_PIN_7;
  GPIO_InitStruct.Alternate = GPIO_AF0_SPI1;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
  /* SPI CS GPIO pin configuration  */
  GPIO_InitStruct.Pin = GPIO_PIN_1;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
  
  RELAY_CS_H();
  
  /* Set the SPI parameters */
  SpiHandle.Instance               = SPI1;
  SpiHandle.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_256;
  SpiHandle.Init.Direction         = SPI_DIRECTION_2LINES;
  SpiHandle.Init.CLKPhase          = SPI_PHASE_1EDGE;
  SpiHandle.Init.CLKPolarity       = SPI_POLARITY_LOW;
  SpiHandle.Init.DataSize          = SPI_DATASIZE_8BIT;
  SpiHandle.Init.FirstBit          = SPI_FIRSTBIT_MSB;
  SpiHandle.Init.TIMode            = SPI_TIMODE_DISABLE;
  SpiHandle.Init.CRCCalculation    = SPI_CRCCALCULATION_DISABLE;
  SpiHandle.Init.CRCPolynomial     = 7;
  SpiHandle.Init.NSS               = SPI_NSS_SOFT;
  SpiHandle.Init.Mode = SPI_MODE_MASTER;
  HAL_SPI_Init(&SpiHandle);
  /* Enable SPI peripheral */
  __HAL_SPI_ENABLE(&SpiHandle);
  
  relay_set(0); /* Disable all relays */
}

/**
 * @brief Set relay status. This function will affect all relays.
 * @param relay_sets: relay settings
 * @return none.
*/
void relay_set(uint32_t relay_bit_set)
{
  volatile uint16_t rd;
  RELAY_CS_L();
  *((__IO uint8_t *)&SPI1->DR)  = (uint8_t)relay_bit_set;
  //wait until all bits sent out.
  while((SPI1->SR & SPI_SR_RXNE) == 0);
  rd = SPI1->DR;
  RELAY_CS_H();
  relay_curr_sta = relay_bit_set;
}

/**
 * @brief Turn on selected relay(s).
 * @param relay_sets: the selected relays are going to be turned on.
 * @return none.
*/
void relay_set_bits(uint32_t relay_sets)
{
  uint32_t temp;
  temp = relay_read_set();
  temp |= relay_sets;
  relay_set(temp);
}

/**
 * @brief Turn off selected relay(s).
 * @param relay_sets: the selected relays are going to be turned off.
 * @return none.
*/
void relay_clr_bits(uint32_t relay_sets)
{
  uint32_t temp;
  temp = relay_read_set();
  temp &=~relay_sets;
  relay_set(temp);
}

/**
 * @brief get curret relay status.
 * @return none.
*/
uint32_t relay_read_set(void)
{
  return relay_curr_sta;
}
