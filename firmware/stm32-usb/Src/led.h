#ifndef _LED_H_
#define _LED_H_

void led_hw_init(void);
void led_disp(uint32_t num);

#endif
