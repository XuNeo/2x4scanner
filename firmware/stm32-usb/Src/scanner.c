#include "scanner.h"
#include "relays.h"
#include "led.h"

#define SCANNER_RESET_ALL (RELAY_2|RELAY_4|RELAY_6|RELAY_7)

static uint32_t scanner_curr_chan = SCANNER_OFF;


static void scanner_delay(void){
  int i=10000;
  while(i--);
}

void scanner_init(void)
{
  relay_hardware_init();
  relay_set(SCANNER_RESET_ALL);
  scanner_delay();
  relay_set(0);
}

static void scanner_reset_chan(uint32_t chan){
  const uint8_t chan2relay_reset[]={0, RELAY_7, RELAY_6, RELAY_4, RELAY_2};
  if(chan<5){
    relay_set(chan2relay_reset[chan]);
    scanner_delay();
    relay_set(0);
  }
}

void scanner_set_chan(uint32_t chan){
  const uint8_t chan2relay_set[]={0, RELAY_8, RELAY_5, RELAY_3, RELAY_1};
  //reset current channel
  scanner_reset_chan(scanner_curr_chan);
  led_disp(chan);
  if(chan > 4)
    return;
  //set channel
  scanner_curr_chan = chan;
  if(chan < 5){
    relay_set(chan2relay_set[chan]);
    scanner_delay();
  }
  relay_set(0); //reset all bits.
}

#include "ush.h"
#include "string.h"

int32_t scanner_set(uint32_t argc, uint8_t *argv[]){
  ush_num_def num_type;
  ush_error_def error;
  uint32_t value;
  if(argc != 2)
    return -1;
  error = ush_str2num(argv[1], strlen((const char*)argv[1]), &num_type, &value);
  if(error != ush_error_ok)
    return -2;
  if(num_type != ush_num_uint32)
    return -3;
  scanner_set_chan(value);
  printf("\nset channel to %d\n", value);
  return 0;
}
USH_REGISTER(scanner_set, chan, select channel between 1~4);

